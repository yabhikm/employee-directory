import axios from 'axios';

const server = axios.create({
  baseURL: 'http://api.additivasia.io',
});

export default server;
