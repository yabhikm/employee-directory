const httpResponse = (status, data) => {
  if (status >= 300 || status < 200) {
    return { response: { status, data } };
  }
  return { status, data, statusText: 'OK' };
};

export {
  httpResponse
};
