import requestService from '../services/requestService';

const search = (query) => {
  const request = {
    method: 'get',
    url: `/api/v1/assignment/employees/${query.name}`,
  };

  return requestService(query, request);
};

export default {
  search,
};
