import { httpResponse } from '../helpers';
import server from '../server';

export default (query, request) =>
  new Promise((resolve, reject) => {
    server({
      ...request
    })
    .then(response => {
      if (response.status === 200) {
        const data = response.data;
        resolve(httpResponse(200, data));
      } else {
        const error = `${response.message || `Something went wrong `}`;
        reject(
          httpResponse(400, {
            message: error,
          }),
        );
      }
    }).catch(err => reject(err));
  });
