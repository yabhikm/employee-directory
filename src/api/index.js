import routes from './routes';

const router = method => (path, params = {}) => {
  const route = (routes[method] || {})[path];
  if (route) {
    return route(params);
  }
};

const get = router('get');

const Api = {
  get
};

export default Api;
