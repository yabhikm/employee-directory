import React, { useState } from 'react';
import {
  makeStyles,
  Paper,
  Typography,
  Grid
} from '@material-ui/core';
import api from '../api';

import Form from './Form';
import EmployeeList from './EmployeeList';
import Detail from './EmployeeDetail';

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  }
}));

const getResultComponent = props => {
  const {
    resultType,
    result,
    onNameSelect
  } = props;
  switch (resultType) {
    
    case 'list':
      return <EmployeeList
        result={result}
        onNameSelect={onNameSelect}
      />;
    
    case 'detail':
      return <Detail
        result={result}
        onNameSelect={onNameSelect}
      />;

    default:
  }
}

const EmployeeDirectoryApp = () => {
  const classes = useStyles();
  const [isResultReady, setIsResultReady] = useState(false);
  const [resultType, setResultType] = useState('list');
  const [result, setResult] = useState([]);
  
  const onSearch = name => {
    setIsResultReady(false);
    setResult([]);
    setResultType(name ? 'detail' : 'list');
    api.get('/search/:name',{ name }).then(res => {
      setIsResultReady(true);
      setResult(res.data || []);
    }).catch(err => {
      setIsResultReady(true);
      setResult([]);
    });
  };

  const onClear = () => {
    setIsResultReady(false);
    setResult([]);
    setResultType('list');
  }

  const onNameSelect = name => {
    onSearch(name);
  }

  return (
    <Paper className={classes.paper}>
      <Typography component="h1" variant="h4" align="center">
        Search Employee
      </Typography>
      <Form
        onSearch={onSearch}
        onClear={onClear}
      />
      {
        isResultReady && (<React.Fragment>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={3}></Grid>
            <Grid item xs={12} sm={6}>
              {
                getResultComponent({resultType, result, onNameSelect})
              }
            </Grid>
            <Grid item xs={12} sm={3}></Grid>
          </Grid>
        </React.Fragment>)
      }
    </Paper>
  );
}

export default EmployeeDirectoryApp;
