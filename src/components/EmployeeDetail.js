import React from 'react';
import {
  makeStyles,
  Card,
  CardContent,
  Typography
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  }
}));

const EmployeeDetail = props => {
  const classes = useStyles();
  const key = 'direct-subordinates';
  const { result } = props;
  return (
    <Card className={classes.card}>
      <CardContent>
        {result.length < 2 ? (
          <Card className={classes.card}>
            <CardContent>
              <Typography variant="h5" component="h2">
                No record found
              </Typography>
            </CardContent>
          </Card>
        ) : (
          <React.Fragment>
            <Typography className={classes.title} color="textSecondary" gutterBottom>
              {result[0]}
            </Typography>
            <Typography variant="h5" component="h2">
              Direct Subordinates
            </Typography>
            {
              result[1][key].map(i =>{
                return (<Typography key={i} className={classes.pos} color="textSecondary">
                  {i}
                </Typography>);
              })
            }
          </React.Fragment>
        )}
      </CardContent>
    </Card>
  );
}

export default EmployeeDetail;
