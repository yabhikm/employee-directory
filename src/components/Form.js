import React, { useState } from 'react';
import {
  makeStyles,
  Grid,
  FormControl,
  Input,
  Button
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  formWrap: {
    padding: theme.spacing(3, 0, 5),
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
  },
}));

const Form = props => {
  const classes = useStyles();
  const [name, setName] = useState('');

  const handleChange = e => {
    setName(e.target.value);
  };

  const handleSearch = () => {
    props.onSearch(name);
  };

  const handleClear = () => {
    setName('');
    props.onClear();
  }

  return (
    <Grid container spacing={3} className={classes.formWrap}>
      <Grid item xs={12} sm={3}></Grid>
      <Grid item xs={12} sm={6}>
        <FormControl fullWidth>
          <Input
            id="name"
            value={name || ''}
            name="name"
            aria-describedby="name-text"
            fullWidth
            placeholder="Employee Name"
            onChange={handleChange}
          />
        </FormControl>
      </Grid>
      <Grid item xs={12} sm={3}></Grid>
      <Grid item xs={12} sm={5}></Grid>
      <Grid item xs={12} sm={6}>
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={handleSearch}
        >Search</Button>
        <Button
          variant="outlined"
          color="primary"
          className={classes.button}
          onClick={handleClear}
        >Clear</Button>
      </Grid>
    </Grid>
  );
}

export default Form;
