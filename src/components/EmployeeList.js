import React from 'react';
import {
  makeStyles,
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  Table,
  TableBody,
  TableCell,
  TablePagination,
  TableRow,
  Card,
  CardContent,
  Typography
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  }
}));

const EmployeeList = props => {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(2);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  }

  const itemClicked = (e, name) => {
    props.onNameSelect(name);
  }

  return (
    !props.result.length ? (
      <Card className={classes.card}>
        <CardContent>
          <Typography variant="h5" component="h2">
            No records found
          </Typography>
        </CardContent>
      </Card>
    ) : (
      <List className={classes.root}>
        {
          props.result.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(i => {
            return (
              <React.Fragment key={i}>
                <Table>
                  <TableBody>
                    <TableRow>
                      <TableCell align="center">
                        <ListItem alignItems="flex-start" onClick={e => itemClicked(e, i)}>
                          <ListItemAvatar>
                            <Avatar alt={i} src="" />
                          </ListItemAvatar>
                          <ListItemText
                            primary={i}
                          />
                        </ListItem>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </React.Fragment>
            );
          })
        }
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={props.result.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </List>
    )
  );
}

export default EmployeeList;
