import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';

import Header from './components/Header';
import EmployeeDirectoryApp from './components/EmployeeDirectoryApp';

function App() {
  return (
    <React.Fragment>
      <CssBaseline />
      <Header />
      <EmployeeDirectoryApp />
    </React.Fragment>
  );
}

export default App;
